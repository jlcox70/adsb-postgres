--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8 (Debian 11.8-1.pgdg90+1)
-- Dumped by pg_dump version 15.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adsb; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA adsb;


ALTER SCHEMA adsb OWNER TO postgres;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


SET default_tablespace = '';

--
-- Name: adsb_messages; Type: TABLE; Schema: adsb; Owner: postgres
--

CREATE TABLE adsb.adsb_messages (
    message_type text,
    transmission_type integer NOT NULL,
    session_id text,
    aircraft_id text,
    hex_ident text NOT NULL,
    flight_id text,
    generated_date date,
    generated_time text,
    logged_date date,
    logged_time text,
    callsign text,
    altitude integer,
    ground_speed integer,
    track integer,
    lat real,
    lon real,
    vertical_rate real,
    squawk text,
    alert integer,
    emergency integer,
    spi integer,
    is_on_ground integer,
    parsed_time timestamp with time zone DEFAULT timezone('utc'::text, now()) NOT NULL
)
WITH (autovacuum_vacuum_scale_factor='0.0', autovacuum_analyze_scale_factor='0.0', autovacuum_analyze_threshold='5000', autovacuum_vacuum_threshold='5000');


ALTER TABLE adsb.adsb_messages OWNER TO postgres;

--
-- Name: callsigns; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.callsigns AS
 SELECT adsb_messages.callsign,
    adsb_messages.hex_ident,
    (adsb_messages.parsed_time)::date AS date_seen,
    max(adsb_messages.parsed_time) AS last_seen,
    min(adsb_messages.parsed_time) AS first_seen
   FROM adsb.adsb_messages
  WHERE (adsb_messages.callsign <> ''::text)
  GROUP BY adsb_messages.callsign, adsb_messages.hex_ident, ((adsb_messages.parsed_time)::date)
  ORDER BY (max(adsb_messages.parsed_time));


ALTER TABLE adsb.callsigns OWNER TO postgres;

--
-- Name: callsigns_last_24_hours; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.callsigns_last_24_hours AS
 SELECT adsb_messages.callsign,
    adsb_messages.hex_ident,
    max(adsb_messages.parsed_time) AS last_seen,
    min(adsb_messages.parsed_time) AS first_seen
   FROM adsb.adsb_messages
  WHERE ((adsb_messages.callsign <> ''::text) AND (adsb_messages.parsed_time >= (CURRENT_TIMESTAMP - '24:00:00'::interval)))
  GROUP BY adsb_messages.callsign, adsb_messages.hex_ident
  ORDER BY (max(adsb_messages.parsed_time));


ALTER TABLE adsb.callsigns_last_24_hours OWNER TO postgres;

--
-- Name: locations; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.locations AS
 SELECT adsb_messages.hex_ident,
    adsb_messages.parsed_time,
    adsb_messages.lon,
    adsb_messages.lat,
    adsb_messages.altitude
   FROM adsb.adsb_messages
  WHERE (adsb_messages.lat IS NOT NULL);


ALTER TABLE adsb.locations OWNER TO postgres;

--
-- Name: flights; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.flights AS
 SELECT DISTINCT l.hex_ident,
    l.parsed_time,
    l.lon,
    l.lat,
    l.altitude,
    cs.callsign
   FROM (adsb.locations l
     JOIN adsb.callsigns cs ON (((l.hex_ident = cs.hex_ident) AND (l.parsed_time <= (cs.last_seen + '00:10:00'::interval)) AND (l.parsed_time >= (cs.first_seen - '00:10:00'::interval)))))
  WHERE ((l.lon <> (0)::double precision) AND (l.lat <> (0)::double precision));


ALTER TABLE adsb.flights OWNER TO postgres;

--
-- Name: last_locations; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.last_locations AS
 SELECT ranked_locations.callsign,
    ranked_locations.hex_ident,
    ranked_locations.parsed_time,
    ranked_locations.lon,
    ranked_locations.lat,
    ranked_locations.altitude
   FROM ( SELECT c.callsign,
            l.hex_ident,
            l.parsed_time,
            l.lon,
            l.lat,
            l.altitude,
            row_number() OVER (PARTITION BY c.callsign ORDER BY l.parsed_time DESC) AS rn
           FROM (adsb.callsigns c
             JOIN adsb.locations l ON ((c.hex_ident = l.hex_ident)))
          WHERE ((l.lon <> (0)::double precision) AND (l.lat <> (0)::double precision))) ranked_locations
  WHERE (ranked_locations.rn = 1);


ALTER TABLE adsb.last_locations OWNER TO postgres;

--
-- Name: last_locations_120_seconds; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.last_locations_120_seconds AS
 SELECT ranked_locations.callsign,
    ranked_locations.hex_ident,
    age(now(), ranked_locations.parsed_time) AS age,
    ranked_locations.lon,
    ranked_locations.lat,
    ranked_locations.altitude
   FROM ( SELECT c.callsign,
            l.hex_ident,
            l.parsed_time,
            l.lon,
            l.lat,
            l.altitude,
            row_number() OVER (PARTITION BY c.callsign ORDER BY l.parsed_time DESC) AS rn
           FROM (adsb.callsigns c
             JOIN adsb.locations l ON ((c.hex_ident = l.hex_ident)))
          WHERE ((l.lon <> (0)::double precision) AND (l.lat <> (0)::double precision) AND (l.parsed_time >= (now() - '00:02:00'::interval)))) ranked_locations
  WHERE (ranked_locations.rn = 1);


ALTER TABLE adsb.last_locations_120_seconds OWNER TO postgres;

--
-- Name: last_tracks; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.last_tracks AS
 SELECT DISTINCT ON (l.callsign) l.callsign,
    l.hex_ident,
    l.parsed_time,
    l.lon,
    l.lat,
    l.altitude,
    m.track
   FROM (adsb.last_locations l
     JOIN adsb.adsb_messages m ON ((l.hex_ident = m.hex_ident)))
  WHERE ((m.track IS NOT NULL) AND (m.track <> 0))
  ORDER BY l.callsign, l.parsed_time DESC;


ALTER TABLE adsb.last_tracks OWNER TO postgres;

--
-- Name: last_tracks_120_seconds; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.last_tracks_120_seconds AS
 SELECT DISTINCT ON (l.callsign) l.callsign,
    l.hex_ident,
    l.age,
    l.lon,
    l.lat,
    l.altitude,
    m.track
   FROM (adsb.last_locations_120_seconds l
     JOIN adsb.adsb_messages m ON ((l.hex_ident = m.hex_ident)))
  WHERE ((m.track IS NOT NULL) AND (m.track <> 0))
  ORDER BY l.callsign, l.age DESC;


ALTER TABLE adsb.last_tracks_120_seconds OWNER TO postgres;

--
-- Name: locations_with_callsigns; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.locations_with_callsigns AS
 SELECT c.callsign,
    l.hex_ident,
    l.parsed_time,
    l.lon,
    l.lat,
    l.altitude
   FROM (adsb.callsigns c
     JOIN adsb.locations l ON ((c.hex_ident = l.hex_ident)))
  WHERE ((l.lon <> (0)::double precision) AND (l.lat <> (0)::double precision));


ALTER TABLE adsb.locations_with_callsigns OWNER TO postgres;

--
-- Name: locations_with_callsigns_last_24; Type: VIEW; Schema: adsb; Owner: postgres
--

CREATE VIEW adsb.locations_with_callsigns_last_24 AS
 SELECT c.callsign,
    l.hex_ident,
    l.parsed_time,
    l.lon,
    l.lat,
    l.altitude
   FROM (adsb.callsigns_last_24_hours c
     JOIN adsb.locations l ON ((c.hex_ident = l.hex_ident)))
  WHERE ((l.lon <> (0)::double precision) AND (l.lat <> (0)::double precision));


ALTER TABLE adsb.locations_with_callsigns_last_24 OWNER TO postgres;

--
-- Name: adsb_messages idx_adsb_unique; Type: CONSTRAINT; Schema: adsb; Owner: postgres
--

ALTER TABLE ONLY adsb.adsb_messages
    ADD CONSTRAINT idx_adsb_unique PRIMARY KEY (transmission_type, parsed_time, hex_ident);


--
-- Name: idx_adsb_time; Type: INDEX; Schema: adsb; Owner: postgres
--

CREATE INDEX idx_adsb_time ON adsb.adsb_messages USING brin (parsed_time);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

