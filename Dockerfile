FROM golang:1.20.0 as exporter
ENV GO111MODULE=on
WORKDIR /app
COPY . .

RUN go install 
RUN CGO_ENABLED=0 GOOS=linux go build -o bin/adsb-db ./
#RUN go build

FROM scratch
COPY --from=exporter /app/bin/adsb-db /usr/local/bin/
COPY db.sql /db.sql
CMD ["/usr/local/bin/adsb-db"]
