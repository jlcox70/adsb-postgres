package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

// ADSBMessage represents the structure of ADS-B messages.
type ADSBMessage struct {
	MessageType      string
	TransmissionType int
	SessionID        string
	AircraftID       string
	HexIdent         string
	FlightID         string
	GeneratedDate    string
	GeneratedTime    string
	LoggedDate       string
	LoggedTime       string
	Callsign         string
	Altitude         int
	GroundSpeed      int
	Track            int
	Lat              float64
	Lon              float64
	VerticalRate     float64
	Squawk           string
	Alert            int
	Emergency        int
	SPI              int
	IsOnGround       int
}

var (
	dump1090Host = os.Getenv("DUMP1090HOST")
	dbName       = os.Getenv("PGDATABASE")
	dbSchema     = os.Getenv("PGSCHEMA")
	dbTable      = os.Getenv("PGTABLE")
	dbHost       = os.Getenv("PGHOST")
	dbPort       = os.Getenv("PGPORT")
	dbUser       = os.Getenv("PGUSER")
	dbPassword   = os.Getenv("PGPASSWORD")
	dbSSLMode    = os.Getenv("PGSSLMODE")
	verbose      = os.Getenv("VERBOSE")
	sbsPort      = os.Getenv("SBSPORT")
)

var db *sql.DB

func main() {
	// Connect to the database
	db = connectToDB()
	defer db.Close()

	// Connect to the SBS port
	conn, err := connectToSBS()
	if err != nil {
		fmt.Println("Error connecting to SBS:", err)
		return
	}
	defer conn.Close()

	scanner := bufio.NewScanner(conn)

	for scanner.Scan() {
		message := scanner.Text()
		if verbose == "true" {
			fmt.Println("Received message:", message)
		}

		// Parse the SBS message
		parsedValues := parseSBSMessage(message)
		if verbose == "true" {
			fmt.Println("Parsed values:", parsedValues)
			fmt.Println("number of values:", len(parsedValues))

		}
		// Map the parsed values to the ADSBMessage struct
		var adsbMessage ADSBMessage
		err := mapToStruct(parsedValues, &adsbMessage)
		if err != nil {
			fmt.Println("Error mapping data to struct:", err)
			continue
		}

		// Insert data into the database
		err = insertToDB(adsbMessage)
		if err != nil {
			fmt.Println("Error inserting data:", err)
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading from SBS connection:", err)
	}
}

// Connect to the SBS port
func connectToSBS() (net.Conn, error) {
	conn, err := net.Dial("tcp", dump1090Host+":"+sbsPort)
	if err != nil {
		return nil, err
	}
	fmt.Println("Connected to SBS port")
	return conn, nil
}

// Parse the SBS message and return parsed values as a string slice
func parseSBSMessage(message string) []string {
	return strings.Split(message, ",")
}

// Connect to the database
func connectToDB() *sql.DB {
	if dbSSLMode == "" {
		dbSSLMode = "disable"
	}
	connectionString := fmt.Sprintf("dbname=%s user=%s password=%s host=%s port=%s sslmode=%s", dbName, dbUser, dbPassword, dbHost, dbPort, dbSSLMode)
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		fmt.Println("Error connecting to the database:", err)
		os.Exit(1)
	}
	fmt.Println("Connected to the database")
	return db
}

// Insert data into the database
func insertToDB(message ADSBMessage) error {
	query := fmt.Sprintf(`INSERT INTO %s.%s 
		VALUES (
			%s, %d, %s, %s, %s,
		 	%s, %s, %s, %s, %s,
			%s, %d, %d, %d, %f, %f,
			%f, %s, %d, %d, %d, %d)`,
		dbSchema, dbTable,
		message.MessageType, message.TransmissionType, message.SessionID, message.AircraftID, message.HexIdent,
		message.FlightID, message.GeneratedDate, message.GeneratedTime, message.LoggedDate, message.LoggedTime,
		message.Callsign, message.Altitude, message.GroundSpeed, message.Track, message.Lat, message.Lon,
		message.VerticalRate, message.Squawk, message.Alert, message.Emergency, message.SPI, message.IsOnGround)

	if verbose == "true" {
		fmt.Println("Constructed SQL query:", query)
	}

	_, err := db.Exec(query)

	if err != nil {
		fmt.Println("Error inserting data:", err)
	} else {
		if verbose == "true" {
			fmt.Println("Data inserted successfully:", message)
		}
	}

	return err
}

// mapToStruct maps the values from a string slice to an ADSBMessage struct.
func mapToStruct(values []string, message *ADSBMessage) error {

	message.MessageType = handleNull(values[0])
	message.TransmissionType = parseInt(values[1])
	message.SessionID = handleNull(values[2])
	message.AircraftID = handleNull(values[3])
	message.HexIdent = handleNull(values[4])
	message.FlightID = handleNull(values[5])
	message.GeneratedDate = handleNull(values[6])
	message.GeneratedTime = handleNull(values[7])
	message.LoggedDate = handleNull(values[8])
	message.LoggedTime = handleNull(values[9])
	message.Callsign = handleNull(values[10])
	message.Altitude = parseInt(values[11])
	message.GroundSpeed = parseInt(values[12])
	message.Track = parseInt(values[13])
	message.Lat = parseFloat(values[14])
	message.Lon = parseFloat(values[15])
	message.VerticalRate = parseFloat(values[16])
	message.Squawk = handleNull(values[17])
	message.Alert = parseInt(values[18])
	message.Emergency = parseInt(values[19])
	message.SPI = parseInt(values[20])
	message.IsOnGround = parseInt(values[21])
	return nil
}

// parseInt converts a string to an integer.
func parseInt(s string) int {
	val, err := strconv.Atoi(s)
	if err != nil {
		return 0
	}
	return val
}

// parseFloat converts a string to a float64.
func parseFloat(s string) float64 {
	val, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0.0
	}
	return val
}

// handleNull handles the case where an empty string should be converted to NULL.
func handleNull(s string) string {
	if s == "" {
		return "NULL"
	}
	return "'" + s + "'"
}
